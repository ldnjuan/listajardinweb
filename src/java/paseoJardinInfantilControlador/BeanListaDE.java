/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseoJardinInfantilControlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import modelos.Infante;
import modelos.Nodo;
import modelos.NodoDE;
import modelos.listaDoblementeEnlazada;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import paseoJardinInfantilControlador.utilidades.JsfUtil;

/**
 *
 * @author Juan
 */
@Named(value = "beanListaDE")
@SessionScoped
public class BeanListaDE implements Serializable {
    private boolean deshabilitarNuevo=true;
    listaDoblementeEnlazada listaDE = new listaDoblementeEnlazada();
    private NodoDE infanteMostrar = new NodoDE(new Infante());
    private boolean verTabla = false;
    private int posicion;
    private DefaultDiagramModel model;
    

    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public boolean isVerTabla() {
        return verTabla;
    }

    public void setVerTabla(boolean verTabla) {
        this.verTabla = verTabla;
    }

    public listaDoblementeEnlazada getListaDE() {
        return listaDE;
    }

    public void setListaDE(listaDoblementeEnlazada listaDE) {
        this.listaDE = listaDE;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public NodoDE getInfanteMostrar() {
        return infanteMostrar;
    }

    public void setInfanteMostrar(NodoDE infanteMostrar) {
        this.infanteMostrar = infanteMostrar;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
    
    
    
    /**
     * Creates a new instance of BeanListaDE
     */
    
    @PostConstruct
    public void llenarInfantes()
    {
        listaDE.adicionarNodoDE(new Infante("Sofia",(byte)7,'F'));
        listaDE.adicionarNodoDE(new Infante("Carlos",(byte)2,'M'));
        listaDE.adicionarNodoDE(new Infante("Tatiana",(byte)2,'F'));
        irAlPrimero();
    }
    public BeanListaDE() {
    }
    
    
    public void habilitarCrearInfante()
    {
        deshabilitarNuevo=false;
        infanteMostrar = new NodoDE(new Infante());
    }
    
    public boolean habilitarBtnPrimero()
    {
        if(deshabilitarNuevo == true)
        {
            return !infanteMostrar.getDato().getNombre().equals(listaDE.getCabeza().getDato().getNombre());
        } else {
            return false;
        }
        
    }
    
    public boolean habilitarBtnUltimo()
    {
        if(deshabilitarNuevo == true)
        {
            return infanteMostrar.getSiguiente() != null;
        } else {
            return false;
        }
        
    }
    
    public void guardarInfante()
    {
        listaDE.adicionarNodoDE(infanteMostrar.getDato());
        infanteMostrar = new NodoDE(new Infante());
        deshabilitarNuevo=true;
        pintarLista();
        irAlPrimero();
        JsfUtil.addSuccessMessage("Se ha adicionado con exito!!");
    }
    
    public void guardarInfanteAlInicio()
    {
        listaDE.adicionarNodoDEAlInicio(infanteMostrar.getDato());
        infanteMostrar = new NodoDE(new Infante());
        deshabilitarNuevo=true;
        irAlPrimero();
    }
    
    public void irAlPrimero() 
    {
        infanteMostrar=listaDE.getCabeza();
        pintarLista();
    }
    
    public void irAlSiguiente() 
    {
        if (infanteMostrar.getSiguiente() != null) {
            infanteMostrar = infanteMostrar.getSiguiente();
        }
        pintarLista();
    }
    
    public void irAlAnterior() 
    {
        if (infanteMostrar.getAnterior()!= null) {
            infanteMostrar = infanteMostrar.getAnterior();
        }
        pintarLista();
    }
    
    public void irAlUltimo()
    {
        infanteMostrar=listaDE.irAlUltimo();
        pintarLista();
    }
    
    public void invertirLista()
    {
        listaDE.invertirLista();
        pintarLista();
        irAlPrimero();
    }
    
    public void cancelarGuardado(){
       deshabilitarNuevo=true;
       pintarLista();
       irAlPrimero();
    }
   
   public void visualizarTabla(){
       if(verTabla == false){
            verTabla=true; 
       } else {
            verTabla=false; 
       }
   }
   
   public void eliminarInfante(){
       listaDE.eliminarNodoDE(infanteMostrar.getDato());
       pintarLista();
       irAlPrimero();
   }
   
   public void eliminarInfantePosicion(){
       listaDE.eliminarNodoDEPosicion(posicion);
       pintarLista();
       irAlPrimero();
       
   }
   
   public void pintarLista() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
         
        StateMachineConnector connector = new StateMachineConnector();
        connector.setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
        connector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
        model.setDefaultConnector(connector);
        
        NodoDE temp = listaDE.getCabeza();
        int posX=5;
        int posY=2;
        while(temp != null)
        {
            Element ele = new Element(temp.getDato().getNombre(),posX+"em", posY+"em");
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));

            model.addElement(ele);
            temp = temp.getSiguiente();
            posX=posX+15;
            posY=posY+10;
            
            for (int i = 0; i < model.getElements().size()-1; i++) 
            {
                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1),
                model.getElements().get(i+1).getEndPoints().get(0), "Sig"));
                
                model.connect(createConnection(model.getElements().get(i+1).getEndPoints().get(2),
                model.getElements().get(i).getEndPoints().get(3), "Ant"));
               
            }
        }
         
    }
     
    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));
         
        if(label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }
         
        return conn;
    }
}
