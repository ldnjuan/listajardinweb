/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseoJardinInfantilControlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import paseoJardinInfantilControlador.utilidades.JsfUtil;

/**
 *
 * @author Comercial
 */
@Named(value = "beanLogin")
@SessionScoped
public class BeanLogin implements Serializable {
    private String usuario;

    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {    
        this.usuario = usuario;
    }

    public BeanLogin() {
    }
    
    public String ingresarSE() 
    {
        if (usuario.equals("juanMa")) {
            return "irListaSE";
        } else {
            JsfUtil.addErrorMessage("Usted no esta autorizado");
            return null;
        }
    }
    
    public String ingresarDE() 
    {
        if (usuario.equals("juanMa")) {
            return "irListaDE";
        } else {
            JsfUtil.addErrorMessage("Usted no esta autorizado");
            return null;
        }
    }
    
    public String ingresarDECircular() 
    {
        if (usuario.equals("juanMa")) {
            return "irListaDECircular";
        } else {
            JsfUtil.addErrorMessage("Usted no esta autorizado");
            return null;
        }
    }
}
