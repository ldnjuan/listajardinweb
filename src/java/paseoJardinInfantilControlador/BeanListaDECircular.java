/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseoJardinInfantilControlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import modelos.Infante;
import modelos.ListaDECircular;
import modelos.NodoDE;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.connector.FlowChartConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import paseoJardinInfantilControlador.utilidades.JsfUtil;

/**
 *
 * @author Juan
 */
@Named(value = "beanListaDECircular")
@SessionScoped
public class BeanListaDECircular implements Serializable {

    private boolean deshabilitarNuevo=true;
    ListaDECircular listaDECircular = new ListaDECircular();
    private NodoDE infanteMostrar = new NodoDE(new Infante());
    private boolean verTabla = false;
    private int posicion;
    private DefaultDiagramModel model;

    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public boolean isVerTabla() {
        return verTabla;
    }

    public void setVerTabla(boolean verTabla) {
        this.verTabla = verTabla;
    }


    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public NodoDE getInfanteMostrar() {
        return infanteMostrar;
    }

    public void setInfanteMostrar(NodoDE infanteMostrar) {
        this.infanteMostrar = infanteMostrar;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public ListaDECircular getListaDECircular() {
        return listaDECircular;
    }

    public void setListaDECircular(ListaDECircular listaDECircular) {
        this.listaDECircular = listaDECircular;
    }
    
    
    
    /**
     * Creates a new instance of BeanListaDE
     */
    
    @PostConstruct
    public void llenarInfantes()
    {
        listaDECircular.adicionarNodoDECircular(new Infante("Sofia",(byte)7,'F'),false);
        listaDECircular.adicionarNodoDECircular(new Infante("Juan",(byte)3,'M'),false);
        listaDECircular.adicionarNodoDECircular(new Infante("Tatiana",(byte)2,'F'),false);
        irAlPrimero();
    }
    
    
    public void habilitarCrearInfante()
    {
        deshabilitarNuevo=false;
        infanteMostrar = new NodoDE(new Infante());
    }
    
    public void guardarInfante(boolean esCabeza)
    {
        listaDECircular.adicionarNodoDECircular(infanteMostrar.getDato(),esCabeza);
        infanteMostrar = new NodoDE(new Infante());
        deshabilitarNuevo=true;
        irAlPrimero();
        JsfUtil.addSuccessMessage("Se ha adicionado con exito!!");
    }
    
    
    public void irAlPrimero() 
    {
        infanteMostrar=listaDECircular.getCabeza();
        pintarLista();
    }
    
    public void irAlSiguiente() 
    {
        if (infanteMostrar.getSiguiente() != null) {
            infanteMostrar = infanteMostrar.getSiguiente();
        }
    }
    
    public void irAlAnterior() 
    {
        if (infanteMostrar.getAnterior()!= null) {
            infanteMostrar = infanteMostrar.getAnterior();
        }
    }
    
    public void irAlUltimo()
    {
        infanteMostrar=listaDECircular.irAlUltimo();
    }
    
    
    public void cancelarGuardado(){
       deshabilitarNuevo=true;
       irAlPrimero();
    }
   
   public void visualizarTabla(){
       if(verTabla == false){
            verTabla=true; 
       } else {
            verTabla=false; 
       }
   }
   
   public void eliminarInfante(){
       listaDECircular.eliminarNodoDECircular(infanteMostrar.getDato());
       irAlPrimero();
   }
//   
//   public void eliminarInfantePosicion(){
//       listaDE.eliminarNodoDEPosicion(posicion);
//       irAlPrimero();
//       
//   }
   
   public void pintarLista() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
         
        
        FlowChartConnector connector = new FlowChartConnector();
        connector.setPaintStyle("{strokeStyle:'#C7B097',lineWidth:3}");
        model.setDefaultConnector(connector);
        
        NodoDE temp = listaDECircular.getCabeza();
        int posX=5;
        int posY=2;
        do {           
            Element ele = new Element(temp.getDato().getNombre(),posX+"em", posY+"em");
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
            model.addElement(ele);
            temp = temp.getSiguiente();

            posX=posX+15;
            posY=posY+10;
            

        } while (temp != listaDECircular.getCabeza());
        
        for (int i = 0; i < model.getElements().size(); i++) 
        {
            if(model.getElements().size() == 1) {

                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1),
                model.getElements().get(i).getEndPoints().get(3), "Sig"));

                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(2),
                model.getElements().get(i).getEndPoints().get(0), "Ant")); 

            } else if(model.getElements().size() > 1 && i < model.getElements().size()-1) {

                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1),
                model.getElements().get(i+1).getEndPoints().get(0), "Sig"));

                model.connect(createConnection(model.getElements().get(i+1).getEndPoints().get(2),
                model.getElements().get(i).getEndPoints().get(3), "Ant"));

            } else if(i == model.getElements().size()-1) {
                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1),
                model.getElements().get(0).getEndPoints().get(0), "Sig"));
                
                model.connect(createConnection(model.getElements().get(0).getEndPoints().get(2),
                model.getElements().get(i).getEndPoints().get(3), "Ant"));
            }
         }
 
    }
     
    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));
         
        if(label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }
         
        return conn;
    }
    
}
