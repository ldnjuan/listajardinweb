/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseoJardinInfantilControlador;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;
import modelos.Infante;
import modelos.Nodo;
import modelos.listaSimplementeEnlazada;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import paseoJardinInfantilControlador.utilidades.JsfUtil;

/**
 *
 * @author aula228
 */
@ManagedBean
@Named(value = "beanJardinInfantil")
@SessionScoped
public class BeanJardinInfantil implements Serializable {
    private boolean deshabilitarNuevo=true;
    listaSimplementeEnlazada ListaSE = new listaSimplementeEnlazada();
    private Nodo infanteMostrar = new Nodo(new Infante());
    private boolean verTabla = false;
    private int posicion;
    private DefaultDiagramModel model;

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }
    

    /**
    * Creates a new instance of BeanJardinInfantil1
    */
    public BeanJardinInfantil() {
    }
    @PostConstruct
    public void llenarInfantes()
    {
        ListaSE.adicionarNodo(new Infante("Rose",(byte)7,'F'));
        ListaSE.adicionarNodo(new Infante("Juan",(byte)2,'M'));
        ListaSE.adicionarNodo(new Infante("Leidy",(byte)2,'F'));
        irAlPrimero();
        pintarLista();
    }
    
    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }
 
    public listaSimplementeEnlazada getListaSE() {
        return ListaSE;
    }

    public void setListaSE(listaSimplementeEnlazada ListaSE) {
        this.ListaSE = ListaSE;
    }

    public Nodo getInfanteMostrar() {
        return infanteMostrar;
    }

    public void setInfanteMostrar(Nodo infanteMostrar) {
        this.infanteMostrar = infanteMostrar;
    }

    public boolean isVerTabla() {
        return verTabla;
    }

    public void setVerTabla(boolean verTabla) {
        this.verTabla = verTabla;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
    
    
    public void habilitarCrearInfante()
    {
        deshabilitarNuevo=false;
        infanteMostrar = new Nodo(new Infante());
    }
    
    public boolean habilitarBtnPrimero()
    {
        if(deshabilitarNuevo == true)
        {
            return !infanteMostrar.getDato().getNombre().equals(ListaSE.getCabeza().getDato().getNombre());
        } else {
            return false;
        }
        
    }
    
    public boolean habilitarBtnUltimo()
    {
        if(deshabilitarNuevo == true)
        {
            return infanteMostrar.getSiguiente() != null;
        } else {
            return false;
        }
        
    }
    
    public void guardarInfante()
    {
        ListaSE.adicionarNodo(infanteMostrar.getDato());
        infanteMostrar = new Nodo(new Infante());
        deshabilitarNuevo=true;
        irAlPrimero();
        pintarLista();
        JsfUtil.addSuccessMessage("Se ha adicionado con exito!!");

    }
    
    public void guardarInfanteAlInicio()
    {
        ListaSE.adicionarNodoAlInicio(infanteMostrar.getDato());
        infanteMostrar = new Nodo(new Infante());
        deshabilitarNuevo=true;
        irAlPrimero();
        pintarLista();
    }
    
    public void irAlPrimero() 
    {
        infanteMostrar=ListaSE.getCabeza();
        pintarLista();
        
    }
    
    public void irAlSiguiente() 
    {
        if (infanteMostrar.getSiguiente() != null) {
            infanteMostrar = infanteMostrar.getSiguiente();
        }
        pintarLista(); 
    }
    
    public void irAlUltimo() 
    {
        infanteMostrar = ListaSE.irAlUltimo();
        pintarLista();
    }
    
    public void invertirLista()
    {
        ListaSE.invertirLista();
        irAlPrimero();
    }
    
    public void cancelarGuardado(){
       deshabilitarNuevo=true;
       irAlPrimero();
    }
   
   public void visualizarTabla(){
       if(verTabla == false){
            verTabla=true; 
       } else {
            verTabla=false; 
       }
   }
   
   public void codigoImpar()
   {
       int conteo = ListaSE.contarNodo();
       if (conteo % 2 != 0) {
           double posicion = conteo/2;
           Math.ceil(posicion);
       }
   }
  
   public void eliminarInfante(){
       if(infanteMostrar.getDato() != null)
       {
           ListaSE.eliminarNodo(infanteMostrar.getDato());
           irAlPrimero();
       }
       
   }
   
   public void eliminarInfantePosicion(){
       ListaSE.eliminarNodoPosicion(posicion);
       irAlPrimero();
   }
   
   public void menoresTresAnos() {
       ListaSE.modificarListaMenores();
       irAlPrimero();
   }
   
//   public void eliminarPosicion()
//   {
//       ListaSE.eliminarNodoPosicion(posicion);
//       irAlPrimero();
//   }
   
   public void pintarLista() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
         
        StateMachineConnector connector = new StateMachineConnector();
        connector.setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
        connector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
        model.setDefaultConnector(connector);
        
        Nodo temp = ListaSE.getCabeza();
        int posX=5;
        int posY=2;
        while(temp != null)
        {
            Element ele = new Element(temp.getDato().getNombre(),posX+"em", posY+"em");
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
            ele.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT));
            model.addElement(ele);
            temp = temp.getSiguiente();
            posX=posX+15;
            posY=posY+10;
            
            for (int i = 0; i < model.getElements().size()-1; i++) 
            {
                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1),
                model.getElements().get(i+1).getEndPoints().get(0), "Sig"));
            }
        }
         
    }
     
    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));
         
        if(label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }
         
        return conn;
    }
 
    
    
}
